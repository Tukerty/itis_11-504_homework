package Programm;

import java.util.Scanner;

public class twoArrays {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("First Array Length:");
        int n = sc.nextInt();
        System.out.println("Second Array Length:");
        int m = sc.nextInt();
        int[] m1 = new int[n];
        int[] m2 = new int[m];
        int[] m3 = new int[m + n];
        int i = 0;
        int j = 0;
        int k = 0;
        System.out.println("First Array:");
        for (int q = 0; q < n; q++) {
            m1[q] = sc.nextInt();
        }
        System.out.println("Second Array:");
        for (int q = 0; q < m; q++) {
            m2[q] = sc.nextInt();
        }
        while (i < n & j < m) {
            if (m1[i] >= m2[j]) {
                m3[k] = m2[j];
                j = j + 1;
            } else {
                m3[k] = m1[i];
                i = i + 1;
            }
            k = k + 1;
        }
        if (i < n) {
            for (int q = i; q < n; q++) {
                m3[k] = m1[q];
                k = k + 1;
            }
        } else {
            for (int q = j; q < m; q++) {
                m3[k] = m2[q];
                k = k + 1;
            }
        }
        for (int q = 0; q < (m + n); q++) {
            System.out.print(m3[q] + " ");
        }
    }
}