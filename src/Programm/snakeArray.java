package Programm;

import java.util.Scanner;

public class snakeArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int xn = 0;
        int yn = 0;
        int n = sc.nextInt();
        int[][] a = new int[n + 1][n + 1];
        int q = 0;
        for (int i = 1; i < (n * 2 - 1); i++) {
            if (i % 2 == 1) {
                xn = 0;
                yn = i + 1;
                for (int t = 1; t <= i; t++) {
                    xn++;
                    yn--;
                    if (xn > 0 && xn <= n && yn > 0 && yn <= n) {
                        q++;
                        a[xn][yn] = q;
                    }
                }
            } else {
                yn = 0;
                xn = i + 1;
                for (int t = 1; t <= i; t++) {
                    yn++;
                    xn--;
                    if (xn > 0 && xn <= n && yn > 0 && yn <= n) {
                        q++;
                        a[xn][yn] = q;
                    }
                }
            }
        }
        a[n][n] = n * n;
        for (int i = 1; i < n + 1; i++) {
            for (int j = 1; j < n + 1; j++) {
                System.out.print(a[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }
    }
}