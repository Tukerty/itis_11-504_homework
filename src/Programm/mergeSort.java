package Programm;

import java.util.Scanner;

public class mergeSort {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cLen = sc.nextInt();
        int[] c = new int[cLen];
        for (int i = 0; i < cLen; i++) {
            c[i] = sc.nextInt();
        }
        c = sort(c);
        System.out.println();
        for (int i = 0; i < c.length; i++) {
            System.out.print(c[i]);
        }
    }

    public static int[] sort(int[] arr){
        if(arr.length < 2) return arr;
        int m = arr.length / 2;
        int[] arr1 = new int[m];
        System.arraycopy(arr, 0, arr1,0,m);
        int[] arr2 = new int[m];
        System.arraycopy(arr, m,arr2,0,m);
        return merge(sort(arr1), sort(arr2));
    }
    public static int[] merge(int[] arr1,int arr2[]) {
        int n = arr1.length + arr2.length;
        int[] arr = new int[n];
        int i1 = 0;
        int i2 = 0;
        for (int i = 0; i < n; i++) {
            if (i1 == arr1.length) {
                arr[i] = arr2[i2++];
            } else if (i2 == arr2.length) {
                arr[i] = arr1[i1++];
            } else {
                if (arr1[i1] < arr2[i2]) {
                    arr[i] = arr1[i1++];
                } else {
                    arr[i] = arr2[i2++];
                }
            }
        }
        return arr;
    }
}