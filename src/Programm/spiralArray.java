package Programm;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class spiralArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int nNum = sc.nextInt();
        int[][] sArray = new int[nNum+1][nNum+1];
        int r = 1;
        int j = 1;
        int k = 1;
        for (int i=1; i<nNum; ++i)
        {
            for (j=1; j<nNum; ++j)
            {
                sArray[i][j] = -1;
            }
        }
        j = 1;
        k = 1;
        for (int i = 1; i < nNum*nNum; ++i)
        {
            int f = 0;
            if (r == 1 && f == 0)
            {
                if (sArray[j][k+1] == -1 && k<nNum)
                {
                    sArray[j][k] = i;
                    k++;
                }
                else
                {
                    sArray[j][k] = i;
                    r = 2;
                    j++;
                    f = 1;
                }
            }
            if (r == 2 && f == 0)
            {
                if (sArray[j+1][k] == -1 &&j<nNum)
                {
                    sArray[j][k] = i;
                    j++;
                }
                else
                {
                    sArray[j][k] = i;
                    r = 3;
                    k--;
                    f = 1;
                }
            }
            if (r == 3 && f == 0)
            {
                if (sArray[j][k-1] == -1 &&j>=0)
                {
                    sArray[j][k] = i;
                    k--;
                }
                else
                {
                    sArray[j][k] = i;
                    r = 4;
                    j--;
                    f = 1;
                }
            }
            if (r == 4 && f == 0)
            {
                if (sArray[j-1][k] == -1 &&j>=0)
                {
                    sArray[j][k] = i;
                    j--;
                }
                else
                {
                    sArray[j][k] = i;
                    r = 1;
                    k++;
                    f = 1;
                }
            }
        }
        for(int i = 1; i<nNum;++i)
        {
            for(j = 1; j<nNum;++j)
            {
                System.out.print(sArray[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }
    }
}