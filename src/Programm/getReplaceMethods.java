package Programm;

import java.util.Scanner;

public class getReplaceMethods {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int[] aArray = new int[a];
        aArray = getArray(a, aArray);
        aArray = sortArray(a, aArray);
        aArray = replaceArray(aArray);
        for (int i = 0; i < aArray.length; i++) {
            System.out.print(aArray[i]);
        }
    }

    public static int[] sortArray(int a, int[] aArray) {
        for (int i = 0; i < a - 1; i++) {
            for (int j = i; j < a; j++) {
                if (aArray[i] > aArray[j])
                {
                    int tmp = aArray[i];
                    aArray[i] = aArray[j];
                    aArray[j] = tmp;
                }
            }
        }
        return aArray;
    }

    public static int[] getArray(int a, int[] aArray) {
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < a; i++) {
            aArray[i] = sc.nextInt();
        }
        return aArray;
    }

    public static int[] replaceArray(int[] aArray) {
        for (int i = 0; i < aArray.length - 1; i = i + 2) {
            int tmp = aArray[i];
            aArray[i] = aArray[i + 1];
            aArray[i + 1] = tmp;
        }
        return aArray;
    }

}