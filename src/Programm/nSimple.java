package Programm;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;

public class nSimple {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int nNum = sc.nextInt();
        List<Integer> x = new ArrayList<Integer>();
        x.add(2);
        int l = 3;
        while (x.size() < -+nNum) {
            boolean f = true;

            for (int i = 0; i < x.size(); ++i) {
                if (l % x.get(i) == 0) {
                    f = false;
                }
            }

            if (f) {
                x.add(l);
            }
            l++;
        }
        System.out.print(x);
    }
}