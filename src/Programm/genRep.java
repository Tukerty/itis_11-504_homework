package Programm;

import java.util.Scanner;

public class genRep {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n = sc.nextInt();
        int[] a=new int[n];
        for (int i=0;i<n;i++) {
            a[i] = i + 1;
        }
        generate(0,n,a);
    }
    public static void generate(int l, int r, int[] a)
    {
        if (l == r)
        {
            for (int i=0;i<a.length;i++) {
                System.out.print(a[i]);
            }
            System.out.println();
        }
        else {
            for (int i = l; i < r; i++) {
                int y = a[l];
                a[l] = a[i];
                a[i] = y;
                generate(l + 1, r, a);
                y = a[l];
                a[l] = a[i];
                a[i] = y;
            }
        }

    }
}