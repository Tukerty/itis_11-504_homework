package Programm;

import java.util.Scanner;
import java.util.Arrays;
        import java.lang.Character;

public class longSum {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String fSum = sc.nextLine();
        String sSum = sc.nextLine();
        adder(fSum,sSum);
    }

    public static String adder(String fSum, String sSum)
    {
        int len = 0;

        if (sSum.length()>fSum.length())
        {
            len = sSum.length();
        }
        else
        {
            len = fSum.length();
        }

        int[] sArray = new int[len];
        int[] fArray = new int[len];
        int[] oArray = new int[len];
        for (int i = 1; i<len;i++)
        {
            fArray[i] = 0;
            sArray[i] = 0;
        }

        for (int i = 0; i<sSum.length(); i++)
        {
            char f = sSum.charAt(sSum.length()-i-1);
            int  x = Character.getNumericValue(f);
            sArray[i] = x;
        }

        for (int i = 0; i<fSum.length();i++)
        {
            char f = fSum.charAt(fSum.length()-i-1);
            int  x = Character.getNumericValue(f);
            fArray[i] = x;
        }

        sArray = reverse(sArray);
        fArray = reverse(fArray);
        int ost = 0;

        for (int i = len-1; i>=0; i--)
        {
            oArray[i] = (fArray[i]+sArray[i]+ost) % 10;
            ost = (fArray[i]+sArray[i]+ost) / 10;
        }
        String returnString = Arrays.toString(oArray).replace(",", "").replace("[","").replace("]","").replace(" ","").trim();
        String lab = "l";
        if (ost == 1)
        {
            lab = lab.concat(returnString);
        }
        else
        {
            lab = returnString;
        }
        System.out.println(lab);
        return lab;
    }

    public static int[] reverse(int[] data) {
        for (int left = 0, right = data.length - 1; left < right; left++, right--) {
            int temp = data[left];
            data[left]  = data[right];
            data[right] = temp;
        }
        return (data);
    }
}