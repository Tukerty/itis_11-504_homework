package Programm;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;
import java.util.Arrays;
import java.lang.Character;

public class longMul {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String fSum = sc.nextLine();
        String sSum = sc.nextLine();
        String oRes = "0";
        String f = "";
        for (int i = sSum.length()-1; i>=0; i--)
        {
            char aChar = sSum.charAt(i);
            int muler = Character.getNumericValue(aChar);
            oRes = lSum((oRes),lMul(fSum, muler) + f);
            f = f + "0";
        }
        System.out.print(oRes);

    }

    public static String lMul(String sMul, int muler) {
        int len = sMul.length();
        int[] fArray = new int[len+1];
        int[] oArray = new int[len+1];
        for (int i = 0; i < len+1; i++) {
            fArray[i] = 0;
        }

        for (int i = 0; i < sMul.length(); i++) {
            char f = sMul.charAt(sMul.length() - i - 1);
            int x = Character.getNumericValue(f);
            fArray[i] = x;
        }

        int ost = 0;
        fArray = reverse(fArray);
        for (int i = len; i>=0;i--)
        {
            oArray[i] = (fArray[i]*muler+ost) % 10;
            ost = (fArray[i]*muler+ost) / 10;
        }

        String formatedString = Arrays.toString(oArray);
        formatedString = formatedString.replace(",", "").replace("[", "").replace("]", "").replace(" ","").trim();
        if (formatedString.charAt(0) == '0')
        {
            formatedString = formatedString.substring(1,formatedString.length());
        }
        return formatedString;
    }

    public static String lSum(String sSum, String fSum) {

        int len = 0;

        if (sSum.length() > fSum.length()) {
            len = sSum.length();
        } else {
            len = fSum.length();
        }

        int[] sArray = new int[len];
        int[] fArray = new int[len];
        int[] oArray = new int[len];
        for (int i = 1; i < len; i++) {
            fArray[i] = 0;
            sArray[i] = 0;
        }

        for (int i = 0; i < sSum.length(); i++) {
            char f = sSum.charAt(sSum.length() - i - 1);
            int x = Character.getNumericValue(f);
            sArray[i] = x;
        }

        for (int i = 0; i < fSum.length(); i++) {
            char f = fSum.charAt(fSum.length() - i - 1);
            int x = Character.getNumericValue(f);
            fArray[i] = x;
        }

        sArray = reverse(sArray);
        fArray = reverse(fArray);
        int ost = 0;

        for (int i = len - 1; i >= 0; i--) {
            oArray[i] = (fArray[i] + sArray[i] + ost) % 10;
            ost = (fArray[i] + sArray[i] + ost) / 10;
        }

        String formatedString = Arrays.toString(oArray);
               formatedString = formatedString.replace(",", "").replace("[", "").replace("]", "").replace(" ","").trim();
        if (ost == 1) {
           formatedString = "1" + formatedString;
    }

        return formatedString;


    }

    public static int[] reverse(int[] data) {
        for (int left = 0, right = data.length - 1; left < right; left++, right--) {
            int temp = data[left];
            data[left] = data[right];
            data[right] = temp;
        }
        return (data);
    }
}