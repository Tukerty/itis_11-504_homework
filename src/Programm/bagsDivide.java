package Programm;

import java.util.Arrays;
import java.util.Scanner;

public class bagsDivide {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        double hs = 0;
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
            hs += a[i];
        }
        hs /= 2;
        double minRaz = hs;
        for (int i = n; i > 0; i--) {
            for (int j = 0; j <= n - i; j++) {
                int l[] = Arrays.copyOfRange(a, j, j + i);
                if (Math.abs(sum(l) - (sum(a)-sum(l))) < minRaz)
                {
                    minRaz = Math.abs(sum(l) - (sum(a)-sum(l)));
                }
            }
        }
        System.out.println(minRaz);
    }

    public static int sum(int[] a) {
        int f = 0;
        for (int i = 0; i < a.length; i++) {
            f += a[i];
        }
        return f;
    }
}